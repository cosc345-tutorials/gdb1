.PHONY : all
.PHONY : clean

all : example1 example2 example3 example4

example1: example1.c
	gcc -o $@-nodebug $<
	gcc -g -o $@ $<

example2: example2.c
	gcc -o $@-nodebug $<
	gcc -g -o $@ $<

example3: example3.c
	gcc -o $@-nodebug $<
	gcc -g -o $@ $<

example4: example4.c
	gcc -o $@-nodebug $<
	gcc -g -o $@ $<

clean:
	rm -f example1 example1-nodebug
	rm -f example2 example2-nodebug
	rm -f example3 example3-nodebug
	rm -f example4 example4-nodebug
