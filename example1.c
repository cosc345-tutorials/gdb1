#include <stdlib.h>
#include <stdio.h>

/* Tool to print out the words from the file named fname, separated by
 * single spaces.
 */

int main(int argc, char **argv)
{
  FILE *infile;
  char *input = malloc(255*sizeof(char));
  int status;
  int word_count = 0;

  char *fname = argv[1];
   
  infile = fopen(fname, "r");
  if(infile == NULL)
    {
      char *errtype = "open ";
      char *ftype = "file ";
      printf("Can not %s%s%s\n",errtype,ftype,fname);
    }
  else
    {
      do /* read word from input file fname and print it */
	{
	  status = fscanf(infile, "%s", input);
	  word_count++;
	  printf(input, " ");
	} while(status != -1);
    }
  fclose(infile);
  free(input);

  return 0;
}
