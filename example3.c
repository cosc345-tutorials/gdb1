#include <stdio.h>

long int factorial(int n) {
  return n < 1 ? 1 : n*factorial(n--);
}

void main(int argc, char** argv) {
  int n;
  argc<2 || sscanf(argv[1], "%d", &n) &&
    printf("Factorial of %d = %ld\n", n, factorial(n));
}
