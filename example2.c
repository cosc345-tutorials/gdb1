#include <stdlib.h>
#include <stdio.h>

#define MAX_POINTS 100

/* Find the average 3D position from a list of 3D points */

struct Point {
  double x, y, z;
  char colour;
};
#define SIZEOF_POINT (3*sizeof(double) + sizeof(char))

int main(int argc, char **argv)
{
  FILE *infile;
  int status = 0;
  int num = 0;
  struct Point points[MAX_POINTS];

  char *fname = argv[1];

  infile = fopen(fname, "r");
  if(infile == NULL)
    {
      printf("Can not open file %s\n",fname);
    }
  else
    {
      while(num<MAX_POINTS) /* read points from input file and add
			       them into point structure */
	{
	  double x,y,z;
	  status = fscanf(infile, "%lf %lf %lf", &x, &y, &z);
	  if(status == -1)
	    {
	      break;
	    }
	  points[num].x = x;
	  points[num].y = y;
	  points[num].z = z;
	  num++;
	}

      double average_x = 0.0;
      double average_y = 0.0;
      double average_z = 0.0;
      
      /* Scan points array printing out data and averaging */
      void *scanner = (void *)points;
      for(int i=0; i<num; i++){
	struct Point *point_cursor = (struct Point *)scanner;
	printf("Point: (%f,%f,%f)\n",point_cursor->x,point_cursor->y,point_cursor->z);
	average_x += point_cursor->x;
	average_y += point_cursor->y;
	average_z += point_cursor->z;
	scanner += SIZEOF_POINT;
      }
      average_x /= (double)num;
      average_y /= (double)num;
      average_z /= (double)num;
      printf("Average: (%f,%f,%f)\n",average_x,average_y,average_z);
    }
  fclose(infile);

  return 0;
}
